package Interfata;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

public class FirstPage{

	JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FirstPage window = new FirstPage();
					window.frame.setVisible(true);
					window.frame.setTitle("PDA - Home");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public FirstPage() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 153, 255));
		frame.setBounds(100, 100, 771, 604);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPoliceDepartmentAdministration = DefaultComponentFactory.getInstance().createTitle(" Police Department Administration");
		lblPoliceDepartmentAdministration.setHorizontalAlignment(SwingConstants.CENTER);
		lblPoliceDepartmentAdministration.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 29));
		lblPoliceDepartmentAdministration.setBounds(116, 51, 473, 63);
		//lblPoliceDepartmentAdministration.setIcon(Icon icon);
		frame.getContentPane().add(lblPoliceDepartmentAdministration);
		
		JButton btnLogIn = new JButton("Log In");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnLogIn.setBounds(278, 185, 152, 25);
		frame.getContentPane().add(btnLogIn);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Register.NewScreen();				
			}
		});
		btnRegister.setBounds(278, 242, 152, 25);
		frame.getContentPane().add(btnRegister);
		
		Canvas canvas = new Canvas();
		canvas.setBounds(536, 150, 100, 100);
		frame.getContentPane().add(canvas);
	}
}
