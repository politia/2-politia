package Interfata;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class InterfataInspector extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPanel;
	private JButton addAgent;
	private JButton delAgent;
	private JButton goToChat;
	private JButton goToMap;
	private JButton goLogOut;
	private JButton editAgent;	
	private JButton adaugaSectie;

	public InterfataInspector(){
		setBounds(100, 200, 800, 650);
		setTitle("Inspectorat Polite");
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		contentPanel = new JPanel();
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		addAgent = new JButton("Adaugare Agent");
		addAgent.setBounds(335, 10, 180, 30);
		contentPanel.add(addAgent);
		
		addAgent.addActionListener(new addAgent());
		
		delAgent = new JButton("Sterge Agent");
		delAgent.setBounds(335, 50, 180, 30);
		contentPanel.add(delAgent);
		
		delAgent.addActionListener(new delAgent());
		
		editAgent = new JButton("Editare Agent");
		editAgent.setBounds(335, 90, 180, 30);
		contentPanel.add(editAgent);
		
		editAgent.addActionListener(new editAgent());
		
	    
		adaugaSectie = new JButton("Adaugare Sectie");
	    adaugaSectie.setBounds(335, 170, 180, 30);
		contentPanel.add(adaugaSectie);
		
		adaugaSectie.addActionListener(new adaugaSectie());
		
		goToChat = new JButton("Chat");
		goToChat.setBounds(335, 390, 180, 30);
		contentPanel.add(goToChat);
		
		goToChat.addActionListener(new goToChat());
		
		goToMap = new JButton("Harta");
		goToMap.setBounds(335, 470, 180, 30);
		contentPanel.add(goToMap);
		
		goToMap.addActionListener(new goToMap());
		
		goLogOut = new JButton("Log Out");
		goLogOut.setBounds(335, 430, 180, 30);
		contentPanel.add(goLogOut);
		
		goLogOut.addActionListener(new goLogOut());
		
		JButton cancelButton = new JButton("Iesire");
		cancelButton.setBounds(395, 580, 70, 23);
		contentPanel.add(cancelButton);

		cancelButton.addActionListener(new CancelButton());
	}
	
	class goLogOut implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			new Login();
			dispose();
		}
	}
	
	class addAgent implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			new AddAgent();
		}
	}
	
	class delAgent implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			new DelAgent();
		}
	}
	
	class editAgent implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			new EditAgent();
		}
	}
	
		
	class adaugaSectie implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			new AdaugaSectie();
		}
	}
	
	class goToMap implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						 new MapGUI();						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	class goToChat implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						GoToChat window = new GoToChat();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}
	
	class CancelButton implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			dispose();
		}
	}
}
