package Interfata;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import ConexiuneBD.ConexiuneDB;


public class Register extends ConexiuneDB{

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPasswordField passwordField;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void NewScreen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register window = new Register();
					window.frame.setVisible(true);
					window.frame.setTitle("PDA - Register Form");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Register() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Tahoma", Font.BOLD, 15));
		frame.getContentPane().setEnabled(false);
		frame.getContentPane().setBackground(new Color(153, 153, 255));
		frame.getContentPane().setForeground(new Color(153, 153, 255));
		frame.setBounds(100, 100, 700, 527);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPoliceDepartmentAdministration = new JLabel("Police Department Administration");
		lblPoliceDepartmentAdministration.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 16));
		lblPoliceDepartmentAdministration.setBounds(12, 13, 281, 30);
		frame.getContentPane().add(lblPoliceDepartmentAdministration);
		
		JLabel lblName = new JLabel("Agent ID:");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblName.setBounds(195, 56, 81, 30);
		frame.getContentPane().add(lblName);
		
		JLabel lblNewLabel = new JLabel("First name:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(195, 109, 100, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblLastName.setBounds(195, 154, 100, 16);
		frame.getContentPane().add(lblLastName);
		
		JLabel lblAddress = new JLabel("Username:");
		lblAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAddress.setBounds(195, 201, 81, 16);
		frame.getContentPane().add(lblAddress);
		
		textField = new JTextField();
		textField.setBounds(288, 61, 155, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(288, 109, 155, 22);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(288, 152, 155, 22);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(288, 201, 155, 22);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPassword.setBounds(195, 250, 81, 16);
		frame.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(288, 248, 155, 22);
		frame.getContentPane().add(passwordField);
		
		JLabel lblType = new JLabel("Type:");
		lblType.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblType.setBounds(195, 341, 56, 16);
		frame.getContentPane().add(lblType);
		
		JCheckBox chckbxPdChief = new JCheckBox("PD Chief");
		chckbxPdChief.setBounds(288, 316, 113, 25);
		frame.getContentPane().add(chckbxPdChief);
		
		JCheckBox chckbxPsChief = new JCheckBox("PS Chief");
		chckbxPsChief.setBounds(288, 353, 113, 25);
		frame.getContentPane().add(chckbxPsChief);
		
		JCheckBox chckbxOfficer = new JCheckBox("Officer");
		chckbxOfficer.setBounds(288, 383, 113, 25);
		frame.getContentPane().add(chckbxOfficer);
		
		JButton btnNewButton = new JButton("Register!");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				try {
					Connection myConnection = GetConnection();
					String query = "insert into agentpolitie (id, nume, prenume, username, password, numesectieag, functie ) values (?,?,?,?,?,?,?)";
					PreparedStatement pst = myConnection.prepareStatement(query);
					pst.setString(1, textField.getText());
					pst.setString(2, textField_1.getText());					
					pst.setString(3, textField_2.getText());
					pst.setString(4, textField_3.getText());
					pst.setString(4, passwordField.getText());
					//pst.setString(5, );
					
					
					
					
					
					pst.execute();
					
					JOptionPane.showMessageDialog(null, "Data Saved");
					pst.close();
					

				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(288, 417, 129, 38);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblPsName = new JLabel("PS Name:");
		lblPsName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPsName.setBounds(195, 284, 81, 16);
		frame.getContentPane().add(lblPsName);
		
		textField_4 = new JTextField();
		textField_4.setBounds(288, 283, 155, 22);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
	}
}
