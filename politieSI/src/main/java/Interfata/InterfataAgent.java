package Interfata;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class InterfataAgent extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPanel;
	private JButton goToChat;
	private JButton goToMap;
	private JButton goLogOut;

	public InterfataAgent(){
		setBounds(100, 200, 800, 650);
		setTitle("Inspectorat Polite");
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		contentPanel = new JPanel();
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		goToChat = new JButton("Chat");
		goToChat.setBounds(335, 430, 180, 30);
		contentPanel.add(goToChat);
		
		goToChat.addActionListener(new goToChat());
		
		goLogOut = new JButton("Log Out");
		goLogOut.setBounds(335, 390, 180, 30);
		contentPanel.add(goLogOut);
		
		goLogOut.addActionListener(new goLogOut());

		goToMap = new JButton("Harta");
		goToMap.setBounds(335, 470, 180, 30);
		contentPanel.add(goToMap);
		
		goToMap.addActionListener(new goToMap());
		
		JButton cancelButton = new JButton("Iesire");
		cancelButton.setBounds(395, 580, 70, 23);
		contentPanel.add(cancelButton);

		cancelButton.addActionListener(new CancelButton());
	}
	
	class goLogOut implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			new Login();
			dispose();
		}
	}
	
	class goToMap implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						new MapGUI();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	class goToChat implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						GoToChat window = new GoToChat();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}
	
	class CancelButton implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			dispose();
		}
	}
}
