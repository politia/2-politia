package Interfata;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;

import Clase.SetupMap;
import Operatii.Punct;


public class MapGUI extends JFrame implements MouseListener 
{
	private static final long serialVersionUID = 1L;
	private JTextField longitude = new JTextField();
	private JTextField latitude = new JTextField();

	private WaypointPainter waypointPainter = new WaypointPainter();
	private JXMapKit map;
	public MapGUI() {
		super("PDA Map - needs internet connection");

		this.setVisible(true);
		this.setBounds(150, 150, 850, 650);
		this.setLayout(null);

		map = (JXMapKit) (new SetupMap()).createOpenMap();
		map.setBounds(20, 20, 600, 300);
		map.getMainMap().addMouseListener(this);
		add(map);

		JLabel lblLong = new JLabel("Longitude");
		JLabel lblLat = new JLabel("Latitude");

		lblLong.setBounds(20, 330, 150, 25);
		lblLat.setBounds(180, 330, 150, 25);

		add(lblLong);
		add(lblLat);


		longitude.setBounds(20, 365, 150, 25);
		latitude.setBounds(180, 365, 150, 25);
		add(longitude);
		add(latitude);
		

		waypointPainter.setRenderer(new WaypointRenderer() {
			@Override
			public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
					Waypoint wp) {
				float alpha = 0.6f;
				Color color = new Color(1, 0, 0, alpha); // Red

				g.setColor(color);
				g.fillOval(-5, -5, 10,10);
				g.setColor(Color.BLACK);
				g.drawOval(-5, -5, 10,10);
				
				return true;
			}

		});

	}

	@Override
	public void mouseClicked(MouseEvent e) {

		JXMapViewer m = map.getMainMap();
		GeoPosition g = m.convertPointToGeoPosition(e.getPoint());

		System.out.println(" geo coordinates:  " + g);
		latitude.setText("" + g.getLatitude());
		longitude.setText("" + g.getLongitude());
		
		generatePoints();

	}

	public void moveWaypoint(GeoPosition g) {
		
		if (waypointPainter.getWaypoints().isEmpty())
			waypointPainter.getWaypoints().add(
					new Waypoint(g.getLatitude(), g.getLongitude()));
		else {
			waypointPainter.getWaypoints().add(
					new Waypoint(g.getLatitude(), g.getLongitude()));
		}

		map.getMainMap().setOverlayPainter(waypointPainter);

	}

	public double geoDistance(GeoPosition g1, GeoPosition g2) {
		final int EARTHRADIUS = 6371;
		double deltaLat = Math.toRadians(g1.getLatitude() - g2.getLatitude());
		double deltaLong = Math
				.toRadians(g1.getLongitude() - g2.getLongitude());

		double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
				+ Math.cos(Math.toRadians(g2.getLatitude()))
				* Math.cos(Math.toRadians(g1.getLatitude()))
				* Math.sin(deltaLong / 2) * Math.sin(deltaLong / 2);
		return EARTHRADIUS * 2 * Math.asin(Math.sqrt(a));
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		
		
	}
	public void generatePoints(){
		
		ArrayList<Point2D> p = new ArrayList<Point2D>();
		p=Punct.generateAllPoints();
		for(Point2D punct : p){
			waypointPainter.getWaypoints().add(new Waypoint(punct.getX(), punct.getY()));

		}
		map.getMainMap().setOverlayPainter(waypointPainter);
	}
}
