package Interfata;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FileSearch {

	JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FileSearch window = new FileSearch();
					window.frame.setVisible(true);
					window.frame.setTitle("PDA - Search File");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FileSearch() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 682, 568);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(new Color(153, 153, 255));
		
		JButton btnNewButton = new JButton("Search File");
		btnNewButton.setBounds(55, 168, 97, 25);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Add File");
		btnNewButton_1.setBounds(55, 224, 97, 25);
		frame.getContentPane().add(btnNewButton_1);
		
		table = new JTable();
		table.setBounds(215, 85, 402, 370);
		frame.getContentPane().add(table);
		
		JButton btnNewButton_2 = new JButton("Back");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		btnNewButton_2.setBounds(55, 445, 97, 25);
		frame.getContentPane().add(btnNewButton_2);
		
		JLabel label = new JLabel("Police Department Administration");
		label.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 16));
		label.setBounds(28, 13, 281, 30);
		frame.getContentPane().add(label);
	}
}
