package Interfata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Clase.InspectoratPolitie;
import Operatii.Operatii;

public class AdaugaSectie extends JFrame
{
	private static final long serialVersionUID = 1L;

	private JTextField jtextFieldNume;
	private JTextField jtextFieldAdresa;
	private JTextField jtextFieldSpecific;
	private JTextField jtextFieldLatitudine;
	private JTextField jtextFieldLongitudine;
		
	private JLabel jlabelNume;
	private JLabel jlabelAdresa;
	private JLabel jlabelSpecific;
	private JLabel jlabelNumeIP;
	private JLabel jlabelLatitudine;
	private JLabel jlabelLongitudine;
	
	
	private JComboBox<String> jComboBoxNumeIP;
	
	private JPanel contentPanel;
	private JButton addButton, cancelButton;
	
	public AdaugaSectie()
	{
		setBounds(100, 230, 350, 450);
		setTitle("Adauga Sectie");
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		contentPanel = new JPanel();
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		jlabelNume = new JLabel("Nume Sectie:");
		jlabelNume.setBounds(10, 10, 130, 29);
		contentPanel.add(jlabelNume);
		
		jtextFieldNume = new JTextField();
		jtextFieldNume.setBounds(150, 10, 130, 29);
		contentPanel.add(jtextFieldNume);
		
		jlabelAdresa = new JLabel("Adresa:");
		jlabelAdresa.setBounds(10, 40, 130, 29);
		contentPanel.add(jlabelAdresa);
		
		jtextFieldAdresa = new JTextField();
		jtextFieldAdresa.setBounds(150, 40, 130, 29);
		contentPanel.add(jtextFieldAdresa);
		
		jlabelSpecific = new JLabel("Specific Sectie:");
		jlabelSpecific.setBounds(10, 70, 130, 29);
		contentPanel.add(jlabelSpecific);
		
		jtextFieldSpecific = new JTextField();
		jtextFieldSpecific.setBounds(150, 70, 130, 29);
		contentPanel.add(jtextFieldSpecific);
		
		jlabelNumeIP = new JLabel("Nume Inspector:");
		jlabelNumeIP.setBounds(10, 100, 130, 29);
		contentPanel.add(jlabelNumeIP);
		
		jComboBoxNumeIP = new JComboBox<String>();		
		jComboBoxNumeIP.setBounds(150, 100, 130, 29);
		contentPanel.add(jComboBoxNumeIP);
		

		ArrayList<InspectoratPolitie> model = Operatii.GetAllNumeIP();
		for(InspectoratPolitie insp : model){
			jComboBoxNumeIP.addItem(insp.getNumeIP());
		}
		
		jlabelLatitudine = new JLabel("Latitudine:");
		jlabelLatitudine.setBounds(10, 130, 130, 29);
		contentPanel.add(jlabelLatitudine);
		
		jtextFieldLatitudine = new JTextField();
		jtextFieldLatitudine.setBounds(150, 130, 130, 29);
		contentPanel.add(jtextFieldLatitudine);
		
		jlabelLongitudine = new JLabel("Longitudine:");
		jlabelLongitudine.setBounds(10, 160, 130, 29);
		contentPanel.add(jlabelLongitudine);
		
		jtextFieldLongitudine = new JTextField();
		jtextFieldLongitudine.setBounds(150, 160, 130, 29);
		contentPanel.add(jtextFieldLongitudine);		
		
		
		addButton = new JButton("Adauga");
		addButton.setBounds(100, 340, 70, 29);
		contentPanel.add(addButton);
		
		cancelButton = new JButton("Iesire");
		cancelButton.setBounds(200, 340, 70, 29);
		contentPanel.add(cancelButton);
		
		addButton.addActionListener(new AddButton());
		cancelButton.addActionListener(new CancelButton());
	
	}
	class AddButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			Operatii op = new Operatii();
			op.addSectie(jtextFieldNume.getText(),
					jtextFieldAdresa.getText(),
					jtextFieldSpecific.getText(), 
					String.valueOf(jComboBoxNumeIP.getSelectedItem()),
					Double.parseDouble(jtextFieldLatitudine.getText()), 
					Double.parseDouble(jtextFieldLongitudine.getText()));
			dispose();
		}
		
	}
	class CancelButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			dispose();
		}
		
	}
}