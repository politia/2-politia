package Interfata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Operatii.Operatii;

public class Login extends JFrame
{
	private static final long serialVersionUID = 1L;

	public static Boolean isDispose=false;
	
	private JLabel jlNumeUtilizator;
	private JLabel jlParolaAgent;

	public static JTextField jtNumeUtilizator;
	public static JTextField jtParola;
	
	private JComboBox<Object> jComboBoxGenLogin;
	private JPanel logInAgentPanel;
	
	public Login()
	{
		setBounds(100, 200, 500, 300);
		setTitle("Inspectorat Polite");
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		logInAgentPanel = new JPanel();
		setContentPane(logInAgentPanel);
		logInAgentPanel.setLayout(null);
		
		jlNumeUtilizator = new JLabel("Nume Utilizator:");
		jlNumeUtilizator.setBounds(10, 10, 130, 30);
		logInAgentPanel.add(jlNumeUtilizator);
		
		jtNumeUtilizator = new JTextField();
		jtNumeUtilizator.setBounds(150, 10, 130, 30);
		logInAgentPanel.add(jtNumeUtilizator);
		
		jlParolaAgent = new JLabel("Parola:");
		jlParolaAgent.setBounds(10, 40, 130, 30);
		logInAgentPanel.add(jlParolaAgent);
		
		jtParola = new JPasswordField();
		jtParola.setBounds(150, 40, 130, 30);
		logInAgentPanel.add(jtParola);
		
		jComboBoxGenLogin = new JComboBox<>();
		jComboBoxGenLogin.setBounds(150, 70, 130, 30);
		logInAgentPanel.add(jComboBoxGenLogin);
		
		jComboBoxGenLogin.addItem("Inspector");
		jComboBoxGenLogin.addItem("Agent");
		
		JButton logInButton = new JButton("Conectare");
		logInButton.setBounds(200, 200, 90, 23);
		logInAgentPanel.add(logInButton);

		logInButton.addActionListener(new LogInButton());
		
		JButton cancelButton = new JButton("Iesire");
		cancelButton.setBounds(210, 230, 70, 23);
		logInAgentPanel.add(cancelButton);
		
		cancelButton.addActionListener(new CancelButton());
	}
	
	class CancelButton implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			dispose();
		}
	}
	class LogInButton implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			Operatii login = new Operatii();
			if(jComboBoxGenLogin.getSelectedItem() == "Agent"){
				login.LoginAgent();
			}
			else if(jComboBoxGenLogin.getSelectedItem() == "Inspector"){
				login.LoginInspector();
			}
			if(isDispose==true){
				dispose();
				isDispose = false;
			}
		}
	}
}


