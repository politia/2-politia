package Interfata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Clase.AgentPolitie;
import Operatii.Operatii;

public class DelAgent extends JFrame
{
	private static final long serialVersionUID = 1L;

	private JLabel jlabelNumePrenumeAgent;
	
	private JComboBox<String> jComboBoxAgent;
	
	private JPanel contentPanel;
	private JButton addButton, cancelButton;
	
	public DelAgent()
	{
		setBounds(200, 140, 350, 250);
		setTitle("Sterge Agent");
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		contentPanel = new JPanel();
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		jlabelNumePrenumeAgent = new JLabel("Alegeti agentul: ");
		jlabelNumePrenumeAgent.setBounds(10, 10, 130, 29);
		contentPanel.add(jlabelNumePrenumeAgent);
		
		jComboBoxAgent = new JComboBox<>();
		jComboBoxAgent.setBounds(150, 10, 130, 29);
		contentPanel.add(jComboBoxAgent);
			
		ArrayList<AgentPolitie> model = Operatii.GetAllAgenti();
		for(AgentPolitie sectie : model){
			jComboBoxAgent.addItem(sectie.getNume());
		}
		
		addButton = new JButton("Sterge");
		addButton.setBounds(20, 70, 70, 29);
		contentPanel.add(addButton);
		
		cancelButton = new JButton("Iesire");
		cancelButton.setBounds(110, 70, 70, 29);
		contentPanel.add(cancelButton);
		
		addButton.addActionListener(new DelButton());
		cancelButton.addActionListener(new CancelButton());
	
	}
	class DelButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			Operatii stergeAgent = new Operatii();
			stergeAgent.StergeAgent(String.valueOf(jComboBoxAgent.getSelectedItem()));
			dispose();
		}
		
	}
	class CancelButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			dispose();
		}
		
	}
}