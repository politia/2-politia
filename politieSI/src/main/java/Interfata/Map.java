package Interfata;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Map {

	JFrame frame;
	private JTextField textField;

	public Map() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 153, 250));
		frame.setBounds(100, 100, 753, 528);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(55, 178, 138, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblSearchpoint = new JLabel("SearchPoint");
		lblSearchpoint.setBounds(55, 161, 101, 16);
		frame.getContentPane().add(lblSearchpoint);
		
		JPanel MapPanel = new JPanel();
		MapPanel.setBounds(275, 54, 426, 355);
		frame.getContentPane().add(MapPanel);
		MapPanel.setLayout(new BorderLayout(0, 0));
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(39, 400, 97, 25);
		frame.getContentPane().add(btnBack);
		
		JLabel label = new JLabel("Police Department Administration");
		label.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 16));
		label.setBounds(12, 13, 281, 30);
		frame.getContentPane().add(label);
	}
}
