package Operatii;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Clase.SectiePolitie;
import Clase.AgentPolitie;
import Clase.InspectoratPolitie;
import ConexiuneBD.ConexiuneDB;
import Interfata.InterfataAgent;
import Interfata.InterfataInspector;
import Interfata.Login;

public class Operatii extends ConexiuneDB
{
	public void AdaugaAgent(String id, String username, String password, String nume, String prenume, String adresaAgent, String pregatireProfesionala, String conduitaMorala, int nrCazuriRezolvate, String functie, String numeSectieAg)
	{
		try{
			 Connection myConnection = GetConnection();	
		     String query = "insert into agentpolitie(id, aUsername, aPassword, nume, prenume, andresaAgent, pregatireProfesionala, conduitaMorala, nrCazuriRezolvate, functie, numeSectieAg)" + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		     PreparedStatement preparedStmt = myConnection.prepareStatement(query);
		     preparedStmt.setString(1, id);
		     preparedStmt.setString(2, username);
		     preparedStmt.setString(3, password);
		     preparedStmt.setString(4, nume);
		     preparedStmt.setString(5, prenume);
		     preparedStmt.setString(6, adresaAgent);
		     preparedStmt.setString(7, pregatireProfesionala);
		     preparedStmt.setString(8, conduitaMorala);
		     preparedStmt.setInt(9, nrCazuriRezolvate);
		     preparedStmt.setString(10, functie);
		     preparedStmt.setString(11, numeSectieAg);
		     preparedStmt.execute();
		     myConnection.close();
		}
		catch(Exception e)
		{
			System.out.println("Eroare la adaugare Agent");
		}
	}
	public static ArrayList<SectiePolitie> GetAllSectii()
	{
		ArrayList<SectiePolitie> result = new ArrayList<SectiePolitie>();
		try{
			Connection myConnection = GetConnection();
			if (myConnection != null)
			{
				Statement st = myConnection.createStatement();
				ResultSet rs = st.executeQuery("select NumeSectie from SectiePolitie");
				while (rs.next())
				{
					SectiePolitie sectie = new SectiePolitie();
					sectie.setNumeSectie(rs.getString("NumeSectie"));
					result.add(sectie);
				}
				st.close();
				myConnection.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("A aparut o eroare in extragerea tuturor sectiilor " + e.getMessage());
		}
		return result;
	}
	public void LoginAgent ()
	{
		try{
			Connection myConnection = GetConnection();	
			String query = "select * from AgentPolitie where aUsername=? and aPassword=? ";
			PreparedStatement PS = myConnection.prepareStatement(query);
			PS.setString(1, Login.jtNumeUtilizator.getText());
			PS.setString(2, Login.jtParola.getText());
			ResultSet rs = PS.executeQuery();
			int count = 0;
			while(rs.next())
			{
				count++;
				
			}
			if(count == 1){
				new InterfataAgent();
				Login.isDispose = true;
			}
			else if(count >= 1){
				JOptionPane.showMessageDialog(null, "Username si parola duplicate");
			}
			else{
				JOptionPane.showMessageDialog(null, "Username si parola gresite");
			}			
		}
		catch(Exception e){
			System.out.println("Eroare login" + e.getMessage());
		}
	}
	public void LoginInspector ()
	{
		try{
			Connection myConnection = GetConnection();	
			String query = "select * from InspectoratPolitie where iUsername=? and iPassword=? ";
			PreparedStatement PS = myConnection.prepareStatement(query);
			PS.setString(1, Login.jtNumeUtilizator.getText());
			PS.setString(2, Login.jtParola.getText());
			ResultSet rs = PS.executeQuery();
			int count = 0;
			while(rs.next())
			{
				count++;
				
			}
			if(count == 1){
				new InterfataInspector();
				Login.isDispose = true;
			}
			else if(count >= 1){
				JOptionPane.showMessageDialog(null, "Username si parola duplicate");
			}
			else{
				JOptionPane.showMessageDialog(null, "Username si parola gresite");
			}			
		}
		catch(Exception e){
			System.out.println("Eroare login" + e.getMessage());
		}
	}
	public static ArrayList<AgentPolitie> GetAllAgenti()
	{
		ArrayList<AgentPolitie> result = new ArrayList<AgentPolitie>();
		try{
			Connection myConnection = GetConnection();
			if (myConnection != null)
			{
				Statement st = myConnection.createStatement();
				ResultSet rs = st.executeQuery("select nume from AgentPolitie");
				while (rs.next())
				{
					AgentPolitie agent = new AgentPolitie();
					agent.setNume(rs.getString("nume"));
					result.add(agent);
				}
				st.close();
				myConnection.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("A aparut o eroare in extragerea tuturor agentilor " + e.getMessage());
		}
		return result;
	}
	public void StergeAgent(String nume)
	{
		try
		{
			Connection myConnection = GetConnection();
			String query = "delete from AgentPolitie where nume=?";
			PreparedStatement preparedStmt = myConnection.prepareStatement(query);
			preparedStmt.setString(1, nume);
			preparedStmt.execute();
			myConnection.close();
		}
		catch (Exception e)
		{
			System.out.println("A aparut o eroare la stergerea agentului" + e.getMessage());
		}
	}
	public void EditareAgent(String username, String password, String nume, String prenume, String adresaAgent, String pregatireProfesionala, String conduitaMorala, int nrCazuriRezolvate, String functie, String numeSectieAg, String id)
	{
		try{
			 Connection myConnection = GetConnection();	
		     String query = "update AgentPolitie set aUsername = ?, aPassword = ?, nume = ?, prenume = ?, adresaAgent = ?, pregatireProfesionala = ?, conduitaMorala = ?, nrCazuriRezolvate = ?, functie = ?, numeSectieAg = ? where id = ?";
		     PreparedStatement preparedStmt = myConnection.prepareStatement(query);	     
		     preparedStmt.setString(1, username);
		     preparedStmt.setString(2, password);
		     preparedStmt.setString(3, nume);
		     preparedStmt.setString(4, prenume);
		     preparedStmt.setString(5, adresaAgent);
		     preparedStmt.setString(6, pregatireProfesionala);
		     preparedStmt.setString(7, conduitaMorala);
		     preparedStmt.setInt(8, nrCazuriRezolvate);
		     preparedStmt.setString(9, functie);
		     preparedStmt.setString(10, numeSectieAg);
		     preparedStmt.setString(11, id);
		     preparedStmt.execute();
		     JOptionPane.showMessageDialog(null, "Data Updated");
		     myConnection.close();	     
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static ArrayList<AgentPolitie> GetAllIdAgenti()
	{
		ArrayList<AgentPolitie> result = new ArrayList<AgentPolitie>();
		try{
			Connection myConnection = GetConnection();
			if (myConnection != null)
			{
				Statement st = myConnection.createStatement();
				ResultSet rs = st.executeQuery("select id from AgentPolitie");
				while (rs.next())
				{
					AgentPolitie agent = new AgentPolitie();
					agent.setId(rs.getString("id"));
					result.add(agent);
				}
				st.close();
				myConnection.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("A aparut o eroare in extragerea tuturor id-urilor agentilor " + e.getMessage());
		}
		return result;
	}  
	public static ArrayList<SectiePolitie> GetSectieLL()
	{
		ArrayList<SectiePolitie> result = new ArrayList<SectiePolitie>();
		try{
			Connection myConnection = GetConnection();
			if (myConnection != null)
			{
				Statement st = myConnection.createStatement();
				ResultSet rs = st.executeQuery("select latitudine, longitudine from SectiePolitie");
				while (rs.next())
				{
					SectiePolitie sectie = new SectiePolitie();
					sectie.setLatitudine(rs.getDouble("latitudine"));
					sectie.setLongitudine(rs.getDouble("longitudine"));
					result.add(sectie);
				}
				st.close();
				myConnection.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("A aparut o eroare in extragerea tuturor sectiilor " + e.getMessage());
		}
		return result;
	}
	
	public void addSectie(String nume, String adresa, String specific, String numeIP, double latitudine, double longitudine)
	{
		try{
			 Connection myConnection = GetConnection();	
		     String query = "insert into sectiepolitie(numeSectie, adresaSectie, specificSectie, numeIP, latitudine, longitudine)" + " values (?, ?, ?, ?, ?, ?)";
		     PreparedStatement preparedStmt = myConnection.prepareStatement(query);
		     preparedStmt.setString(1, nume);
		     preparedStmt.setString(2, adresa);
		     preparedStmt.setString(3, specific);
		     preparedStmt.setString(4, numeIP);
		     preparedStmt.setDouble(5, latitudine);
		     preparedStmt.setDouble(6, longitudine);
		     
		     preparedStmt.execute();
		     myConnection.close();
		}
		catch(Exception e)
		{
			System.out.println("Eroare la adaugare Sectie");
		}
	}
	
	public static ArrayList<InspectoratPolitie> GetAllNumeIP()
	{
		ArrayList<InspectoratPolitie> result = new ArrayList<InspectoratPolitie>();
		try{
			Connection myConnection = GetConnection();
			if (myConnection != null)
			{
				Statement st = myConnection.createStatement();
				ResultSet rs = st.executeQuery("select numeIP from InspectoratPolitie");
				while (rs.next())
				{
					InspectoratPolitie ip = new InspectoratPolitie();
					ip.setNumeIP(rs.getString("numeIP"));
					result.add(ip);
				}
				st.close();
				myConnection.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("A aparut o eroare in extragerea numelui inspectoratului " + e.getMessage());
		}
		return result;
	}  

}
