package Operatii;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import Clase.SectiePolitie;

public class Punct {
	
	private static double x;//latitudine
	private static double y;//longitudine
	private String eticheta;
	//private WaypointPainter waypointPainter = new WaypointPainter();
		
	//32 puncte
	
	public Punct(){
		this.x=0;
		this.y=0;
		this.eticheta=null;
	}
	
	public void setCoord(double x, double y){
		this.x=x;
		this.y=y;
	}
	
	public void setLabel(String eticheta){
		this.eticheta=eticheta;
	}
	
	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}
	
	public String getEticheta(){
		return this.eticheta;
	}
	
	public static ArrayList<Point2D> generateAllPoints(){	
		ArrayList<Point2D> puncte = new ArrayList<Point2D>();
		ArrayList<SectiePolitie> model = Operatii.GetSectieLL();
		for(SectiePolitie sectie : model){
			x = sectie.getLatitudine();
			y = sectie.getLongitudine();
			
			puncte.add(new Point2D.Double(x,y));
		}
						
		return puncte;
	}
	
}
