package Clase;

public class AgentPolitie {

	String id;
	String aUsername;
	String aPassword;
	String nume;
	String prenume;
	String adresaAgent;
	String pregatireProfesionala;
	String conduitaMorala;
	int nrCazuriRezolvate;
	String functie;
	String numeSectieAg;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getaUsername() {
		return aUsername;
	}
	public void setaUsername(String aUsername) {
		this.aUsername = aUsername;
	}
	public String getaPassword() {
		return aPassword;
	}
	public void setaPassword(String aPassword) {
		this.aPassword = aPassword;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getAdresaAgent() {
		return adresaAgent;
	}
	public void setAdresaAgent(String adresaAgent) {
		this.adresaAgent = adresaAgent;
	}
	public String getPregatireProfesionala() {
		return pregatireProfesionala;
	}
	public void setPregatireProfesionala(String pregatireProfesionala) {
		this.pregatireProfesionala = pregatireProfesionala;
	}
	public String getConduitaMorala() {
		return conduitaMorala;
	}
	public void setConduitaMorala(String conduitaMorala) {
		this.conduitaMorala = conduitaMorala;
	}
	public int getNrCazuriRezolvate() {
		return nrCazuriRezolvate;
	}
	public void setNrCazuriRezolvate(int nrCazuriRezolvate) {
		this.nrCazuriRezolvate = nrCazuriRezolvate;
	}
	public String getFunctie() {
		return functie;
	}
	public void setFunctie(String functie) {
		this.functie = functie;
	}
	public String getNumeSectieAg() {
		return numeSectieAg;
	}
	public void setNumeSectieAg(String numeSectieAg) {
		this.numeSectieAg = numeSectieAg;
	}

}
