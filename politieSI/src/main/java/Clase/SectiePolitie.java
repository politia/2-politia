package Clase;

public class SectiePolitie {
	String numeSectie;
	String adresaSectie;
	String specificSectie;
	String numeIP;
	double latitudine;
	
	public double getLatitudine() {
		return latitudine;
	}
	public void setLatitudine(double latitudine) {
		this.latitudine = latitudine;
	}
	public double getLongitudine() {
		return longitudine;
	}
	public void setLongitudine(double longitudine) {
		this.longitudine = longitudine;
	}
	double longitudine;
	
	public String getNumeSectie() {
		return numeSectie;
	}
	public void setNumeSectie(String numeSectie) {
		this.numeSectie = numeSectie;
	}
	public String getAdresaSectie() {
		return adresaSectie;
	}
	public void setAdresaSectie(String adresaSectie) {
		this.adresaSectie = adresaSectie;
	}
	public String getSpecificSectie() {
		return specificSectie;
	}
	public void setSpecificSectie(String specificSectie) {
		this.specificSectie = specificSectie;
	}
	public String getNumeIP() {
		return numeIP;
	}
	public void setNumeIP(String numeIP) {
		this.numeIP = numeIP;
	}

}
