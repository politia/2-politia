package Clase;

public class InspectoratPolitie {
	String numeIP;
	String iUsername;
	String iPassword;
	String nume;
	String prenume;
	String adresaInsp;
	String nrTelefon;
	
	public String getNumeIP() {
		return numeIP;
	}
	public void setNumeIP(String numeIP) {
		this.numeIP = numeIP;
	}
	public String getiUsername() {
		return iUsername;
	}
	public void setiUsername(String iUsername) {
		this.iUsername = iUsername;
	}
	public String getiPassword() {
		return iPassword;
	}
	public void setiPassword(String iPassword) {
		this.iPassword = iPassword;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getAdresaInsp() {
		return adresaInsp;
	}
	public void setAdresaInsp(String adresaInsp) {
		this.adresaInsp = adresaInsp;
	}
	public String getNrTelefon() {
		return nrTelefon;
	}
	public void setNrTelefon(String nrTelefon) {
		this.nrTelefon = nrTelefon;
	}
	

}
