package Clase;

import javax.swing.JComponent;

import org.jdesktop.swingx.JXMapKit;

import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.TileFactory;
import org.jdesktop.swingx.mapviewer.TileFactoryInfo;

public class SetupMap {
     
    public JComponent createOpenMap() {
        JXMapKit map = new JXMapKit();
        final int max = 17;
        TileFactoryInfo info = new TileFactoryInfo(1,max-2,max,
                256, true, true, // tile size is 256 and x/y orientation is normal
                "http://tile.openstreetmap.org",
                "x","y","z") {
            public String getTileUrl(int x, int y, int zoom) {
                zoom = max-zoom;
                String url = this.baseURL +"/"+zoom+"/"+x+"/"+y+".png";
                return url;
            }

        };
        TileFactory tf = new DefaultTileFactory(info);
        map.setTileFactory(tf);
        map.setZoom(5);
        map.setAddressLocation(new GeoPosition(44.32762426275736,23.7943696975708));
        
        return map;
    }
    
    
 
}
