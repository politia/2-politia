package Clase;

import java.io.File;

public class Dosar {
	
	String numeDosar;
	String descriere;
	String statusDosar;
	File downloadDosar;
	String numeCateg;
	
	public String getNumeDosar() {
		return numeDosar;
	}
	public void setNumeDosar(String numeDosar) {
		this.numeDosar = numeDosar;
	}
	public String getDescriere() {
		return descriere;
	}
	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}
	public String getStatusDosar() {
		return statusDosar;
	}
	public void setStatusDosar(String statusDosar) {
		this.statusDosar = statusDosar;
	}
	public File getDownloadDosar() {
		return downloadDosar;
	}
	public void setDownloadDosar(File downloadDosar) {
		this.downloadDosar = downloadDosar;
	}
	public String getNumeCateg() {
		return numeCateg;
	}
	public void setNumeCateg(String numeCateg) {
		this.numeCateg = numeCateg;
	}
	
}
