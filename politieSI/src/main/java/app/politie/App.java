package app.politie;

import java.awt.EventQueue;
import java.util.logging.*;

import javax.swing.*;

import Interfata.InterfataInspector;
import Interfata.Login;

public class App {

	public static void main(String[] args) 
	{
		try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InterfataInspector.class.getName()).log(Level.SEVERE, null, ex);   
        } catch (InstantiationException ex) {
            Logger.getLogger(InterfataInspector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
        	Logger.getLogger(InterfataInspector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(InterfataInspector.class.getName()).log(Level.SEVERE, null, ex);
        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Login();
			}
		});
	}
}
