package Interfata;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.TextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class ProfilePage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProfilePage frame = new ProfilePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ProfilePage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 728, 535);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Name = new JLabel("First Name");
		Name.setBounds(55, 75, 87, 16);
		contentPane.add(Name);
		
		JLabel InputFirstName = new JLabel("");
		InputFirstName.setBounds(201, 41, 132, 16);
		contentPane.add(InputFirstName);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(55, 104, 87, 16);
		contentPane.add(lblLastName);
		
		JLabel lblAdress = new JLabel("Adress");
		lblAdress.setBounds(55, 133, 87, 16);
		contentPane.add(lblAdress);
		
		JLabel lblProfessionalTraining = new JLabel("Professional \r\nTraining");
		lblProfessionalTraining.setHorizontalAlignment(SwingConstants.LEFT);
		lblProfessionalTraining.setVerticalAlignment(SwingConstants.TOP);
		lblProfessionalTraining.setBounds(55, 162, 140, 16);
		contentPane.add(lblProfessionalTraining);
		
		JLabel lblMoralEtiquette = new JLabel("Moral Etiquette");
		lblMoralEtiquette.setBounds(55, 220, 105, 16);
		contentPane.add(lblMoralEtiquette);
		
		JLabel lblNumberOfSolved = new JLabel("Number of Solved Cases");
		lblNumberOfSolved.setBounds(55, 191, 140, 16);
		contentPane.add(lblNumberOfSolved);
		
		JLabel lblPoliceDepartmentAdministration = new JLabel("Police Department Administration");
		lblPoliceDepartmentAdministration.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 16));
		lblPoliceDepartmentAdministration.setBounds(12, 13, 281, 30);
		contentPane.add(lblPoliceDepartmentAdministration);
		
		JLabel lblSearchInformation = new JLabel("Search Information");
		lblSearchInformation.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSearchInformation.setBounds(55, 311, 152, 16);
		contentPane.add(lblSearchInformation);
	}
}
