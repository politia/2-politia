package Clase;

public class InspectoratPolitie {
	String NumeIP;
	String IUsername;
	String IPassword;
	String Nume;
	String Prenume;
	String AdresaInsp;
	String NrTelefon;
	
	public String getIUsername() {
		return IUsername;
	}
	public void setIUsername(String iUsername) {
		IUsername = iUsername;
	}
	public String getIPassword() {
		return IPassword;
	}
	public void setIPassword(String iPassword) {
		IPassword = iPassword;
	}
	public String getNumeIP() {
		return NumeIP;
	}
	public void setNumeIP(String numeIP) {
		NumeIP = numeIP;
	}
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	public String getPrenume() {
		return Prenume;
	}
	public void setPrenume(String prenume) {
		Prenume = prenume;
	}
	public String getAdresaInsp() {
		return AdresaInsp;
	}
	public void setAdresaInsp(String adresaInsp) {
		AdresaInsp = adresaInsp;
	}
	public String getNrTelefon() {
		return NrTelefon;
	}
	public void setNrTelefon(String nrTelefon) {
		NrTelefon = nrTelefon;
	}
	
	
}
