package Interfata;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.UIManager;

public class ProfilePage extends JFrame {

	private JPanel contentPane;
	private JTextField Search;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProfilePage frametw = new ProfilePage("Profile Page");
					frametw.setVisible(true);
					frametw.setTitle("PDA - Profile Page");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param string 
	 */
	public ProfilePage(String string) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 728, 535);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Name = new JLabel("First Name");
		Name.setBounds(55, 75, 87, 16);
		contentPane.add(Name);
		
		JLabel InputFirstName = new JLabel("Tom");
		InputFirstName.setFont(new Font("Tahoma", Font.BOLD, 14));
		InputFirstName.setBounds(214, 75, 132, 16);
		contentPane.add(InputFirstName);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(55, 104, 87, 16);
		contentPane.add(lblLastName);
		
		JLabel lblAdress = new JLabel("Adress");
		lblAdress.setBounds(55, 133, 87, 16);
		contentPane.add(lblAdress);
		
		JLabel lblProfessionalTraining = new JLabel("Professional \r\nTraining");
		lblProfessionalTraining.setHorizontalAlignment(SwingConstants.LEFT);
		lblProfessionalTraining.setVerticalAlignment(SwingConstants.TOP);
		lblProfessionalTraining.setBounds(55, 162, 140, 16);
		contentPane.add(lblProfessionalTraining);
		
		JLabel lblMoralEtiquette = new JLabel("Moral Etiquette");
		lblMoralEtiquette.setBounds(55, 220, 105, 16);
		contentPane.add(lblMoralEtiquette);
		
		JLabel lblNumberOfSolved = new JLabel("Number of Solved Cases");
		lblNumberOfSolved.setBounds(55, 191, 140, 16);
		contentPane.add(lblNumberOfSolved);
		
		JLabel lblPoliceDepartmentAdministration = new JLabel("Police Department Administration");
		lblPoliceDepartmentAdministration.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 16));
		lblPoliceDepartmentAdministration.setBounds(12, 13, 281, 30);
		contentPane.add(lblPoliceDepartmentAdministration);
		
		JLabel lblSearchInformation = new JLabel("Search Information");
		lblSearchInformation.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSearchInformation.setBounds(43, 340, 152, 16);
		contentPane.add(lblSearchInformation);
		
		Search = new JTextField();
		Search.setBounds(43, 369, 266, 22);
		contentPane.add(Search);
		Search.setColumns(10);
		
		JLabel InputLastName = new JLabel("Hank");
		InputLastName.setFont(new Font("Tahoma", Font.BOLD, 14));
		InputLastName.setBounds(214, 104, 132, 16);
		contentPane.add(InputLastName);
		
		JLabel InputAddress = new JLabel("Roses Street 16, NY");
		InputAddress.setFont(new Font("Tahoma", Font.BOLD, 14));
		InputAddress.setBounds(214, 133, 146, 16);
		contentPane.add(InputAddress);
		
		JLabel InputPT = new JLabel("Officer");
		InputPT.setFont(new Font("Tahoma", Font.BOLD, 14));
		InputPT.setBounds(214, 162, 132, 16);
		contentPane.add(InputPT);
		
		JLabel InputSC = new JLabel("10");
		InputSC.setFont(new Font("Tahoma", Font.BOLD, 14));
		InputSC.setBounds(214, 191, 132, 16);
		contentPane.add(InputSC);
		
		JLabel InputMoral = new JLabel("Great");
		InputMoral.setFont(new Font("Tahoma", Font.BOLD, 14));
		InputMoral.setBounds(214, 220, 146, 16);
		contentPane.add(InputMoral);
		
		JButton btnNewButton = new JButton("Start Conversation");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GoToChat nw = new GoToChat();
				nw.frame.setVisible(true);		
			}
		});
		btnNewButton.setBounds(479, 158, 152, 25);
		contentPane.add(btnNewButton);
		
		JButton btnSeeMap = new JButton("See Map");
		btnSeeMap.setBounds(479, 231, 152, 25);
		contentPane.add(btnSeeMap);
		btnSeeMap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map nw = new Map();
				nw.frame.setVisible(true);			
			}
		});
		JButton btnManageFiles = new JButton("Manage Files");
		btnManageFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileSearch nw = new FileSearch();
				nw.frame.setVisible(true);	
			}
		});
		btnManageFiles.setBounds(479, 308, 152, 25);
		contentPane.add(btnManageFiles);
		
		JButton btnNewButton_1 = new JButton("Log Out");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Back();
				FirstPage nw = new FirstPage();
				nw.frame.setVisible(true);
			}
			public void Back(){
				//frametw.setVisible(false);
			}
		});
		btnNewButton_1.setBackground(new Color(240, 248, 255));
		btnNewButton_1.setBounds(534, 425, 97, 25);
		contentPane.add(btnNewButton_1);
	}
}
