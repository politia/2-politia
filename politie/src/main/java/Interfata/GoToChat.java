package Interfata;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GoToChat {

	JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GoToChat window = new GoToChat();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GoToChat() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 468, 297);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setBackground(new Color(153, 153, 255));
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Police Department Administration");
		label.setFont(new Font("Segoe UI Semibold", Font.BOLD | Font.ITALIC, 16));
		label.setBounds(28, 13, 281, 30);
		frame.getContentPane().add(label);
		
		JButton btnNewButton = new JButton("Connect Server");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ServerFrame nw = new ServerFrame();
				nw.setVisible(true);
			}
		});
		btnNewButton.setBounds(145, 101, 140, 25);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Chat");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientFrame nw = new ClientFrame();
				nw.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(145, 151, 140, 25);
		frame.getContentPane().add(btnNewButton_1);
		
	}
}
